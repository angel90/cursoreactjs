import React, { Component } from 'react';
import PropTypes from 'prop-types'
import './App.css';
//import ConditionalSection from './sections/Condictions';
//import Forms from './sections/Form';

// props: cargar datos en forma de objeto a un componente.

/*
El estado comienza con un valor predeterminado cuando se monta un componente
luego sufre mutaciones en el tiempo (generadas principalmente por eventos del usuario).
*/


class OtroSaludo extends Component {
  static propTypes = {
    pais: PropTypes.string.isRequired
  }
  constructor(props) {
    super (props);
    this.state = {
      show: true
    }
  }

  estado = () => {
    this.setState({show: !this.state.show})
  }

  render(){
    const {saludo = 'Hola', pais} = this.props;
    if(this.state.show) {
      return(
        <div id = 'componenteSaludo'>
          <h3>Bienveido a: {pais}</h3>
          <p>{saludo}</p>
          <button onClick = {this.estado}>Cambiar estado</button>
        </div>
      );
    } else {
      return (
        <div>
          <h3>No hay elementos</h3>
          <button onClick = {this.estado}>Cambiar estado</button>
        </div>
      );
    }
  }
}

class App extends Component {
  render(){
    return (
      <div>
          <p>Bienvenidos a los primeros pasos con React</p>
          <OtroSaludo saludo = 'Hola' pais = 'España'/>
          <OtroSaludo pais = 'Inglaterra'/>
          <OtroSaludo saludo = 'Salut'/>
      </div>
    );
  }  
}
/*
class App extends Component {
  render(){
    return (
      <div>
          <Forms />
      </div>
    );
  }  
}
*/
export default App;
