import React, { Component } from 'react';


class Login extends Component{
    render(){
        return(
            <div>
                <p>Iniciar Sesion</p>
                <button>Loggin</button>
            </div>
        );
    }
}

class LogOut extends Component{
    render(){
        
        return(
            <div>
                <p>Bienvenido</p>
                <button>LogOut</button>
            </div>
        );
    }
}

export default class ConditionalSection extends Component {
    constructor(){
        super();
        this.state = {Loggin: false};
    }
    render(){
        return(
            <div>
                <h3>Conditional Rendering</h3>
                {this.state.Loggin ? <Login/> : <LogOut/>}
            </div>
        );
    }
}