import React, { Component } from 'react';

export default class Forms extends Component {
    constructor() {
        super();
        this.state = {
            inputName: '',
            inputTwitter: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log(this.state);
    }

    render(){
        return (
            <div>
                <form onSubmit = {this.handleSubmit}>
                    <h4>Formulario</h4>
                    <p>
                        <label htmlFor = 'name'>Nombre: </label> 
                            <input
                                id = 'name'
                                name = 'name'
                                placeholder = 'Introduce un nombre'
                                value = {this.state.inputName}
                                onChange = {event => this.setState({inputName: event.target.value})} /> 
                    </p>
                    <p>
                        <label htmlFor = 'twitter'>Twitter: </label> 
                            <input
                                id = 'twitter'
                                name = 'twitter'
                                placeholder = 'Twitter'
                                value = {this.state.inputTwiter}
                                onChange = {event => this.setState({inputTwitter: event.target.value})} /> 
                    </p>  
                    <button>Enviar</button>
                </form>
            </div>
        )    
    }
}